package com.example.tic_tac_toe

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var firstPlayer = true;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        button00.setOnClickListener {
            checkPlayer(button00)
            checkPlayerWin()
        }
        button01.setOnClickListener {
            checkPlayer(button01)
            checkPlayerWin()
        }
        button02.setOnClickListener {
            checkPlayer(button02)
            checkPlayerWin()
        }
        button10.setOnClickListener {
            checkPlayer(button10)
            checkPlayerWin()
        }
        button11.setOnClickListener {
            checkPlayer(button11)
            checkPlayerWin()
        }
        button12.setOnClickListener {
            checkPlayer(button12)
            checkPlayerWin()
        }
        button20.setOnClickListener {
            checkPlayer(button20)
            checkPlayerWin()
        }
        button21.setOnClickListener {
            checkPlayer(button21)
            checkPlayerWin()
        }
        button22.setOnClickListener {
            checkPlayer(button22)
            checkPlayerWin()
        }

        restart.setOnClickListener {
            restartGame()
        }
    }

    private fun checkPlayer(button: Button) {
        if (button.text.isEmpty())
            if (firstPlayer) {
                button.text = "X"
                firstPlayer = false
            } else {
                button.text = "O"
                firstPlayer = true
            }
    }

    private fun checkPlayerWin() {
        if (button00.text.isNotEmpty() && button00.text == button01.text && button00.text == button02.text) {
            Toast.makeText(this, "Winner is ${button00.text}", Toast.LENGTH_SHORT).show()
            deleteClick()
        } else if (button10.text.isNotEmpty() && button10.text == button11.text && button10.text == button12.text) {
            Toast.makeText(this, "Winner is ${button10.text}", Toast.LENGTH_SHORT).show()
            deleteClick()
        } else if (button20.text.isNotEmpty() && button20.text == button21.text && button20.text == button22.text) {
            Toast.makeText(this, "Winner is ${button20.text}", Toast.LENGTH_SHORT).show()
            deleteClick()
        } else if (button00.text.isNotEmpty() && button00.text == button10.text && button00.text == button20.text) {
            Toast.makeText(this, "Winner is ${button00.text}", Toast.LENGTH_SHORT).show()
            deleteClick()
        } else if (button01.text.isNotEmpty() && button01.text == button11.text && button01.text == button21.text) {
            Toast.makeText(this, "Winner is ${button01.text}", Toast.LENGTH_SHORT).show()
            deleteClick()
        } else if (button02.text.isNotEmpty() && button02.text == button12.text && button02.text == button22.text) {
            Toast.makeText(this, "Winner is ${button02.text}", Toast.LENGTH_SHORT).show()
            deleteClick()
        } else if (button00.text.isNotEmpty() && button00.text == button11.text && button00.text == button22.text) {
            Toast.makeText(this, "Winner is ${button00.text}", Toast.LENGTH_SHORT).show()
            deleteClick()
        } else if (button02.text.isNotEmpty() && button02.text == button11.text && button02.text == button20.text) {
            Toast.makeText(this, "Winner is ${button02.text}", Toast.LENGTH_SHORT).show()
            deleteClick()
        }
    }

    private fun deleteClick() {
        button00.isClickable = false
        button01.isClickable = false
        button02.isClickable = false
        button10.isClickable = false
        button11.isClickable = false
        button12.isClickable = false
        button20.isClickable = false
        button21.isClickable = false
        button22.isClickable = false
    }

    private fun restartGame () {
        button00.isClickable = true
        button00.text = ""
        button01.isClickable = true
        button01.text = ""
        button02.isClickable = true
        button02.text = ""
        button10.isClickable = true
        button10.text = ""
        button11.isClickable = true
        button11.text = ""
        button12.isClickable = true
        button12.text = ""
        button20.isClickable = true
        button20.text = ""
        button21.isClickable = true
        button21.text = ""
        button22.isClickable = true
        button22.text = ""
    }
}

